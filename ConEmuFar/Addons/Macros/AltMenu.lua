Macro {
  area="Shell"; key="Alt"; flags=""; description="Use Alt to activate menu"; action = function()
Keys('F9')
  end;
}

Macro {
  area="Shell"; key="RAlt"; flags=""; description="Use Right Alt to activate menu"; action = function()
Keys('F9')
  end;
}

Macro {
  area="MainMenu"; key="Alt"; flags=""; description="Use Alt to activate menu"; action = function()
Keys('Esc')
  end;
}

Macro {
  area="MainMenu"; key="RAlt"; flags=""; description="Use Right Alt to activate menu"; action = function()
Keys('Esc')
  end;
}

