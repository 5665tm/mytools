Macro {
  area="Editor"; key="AltX"; flags=""; description="Close editor"; action = function()
Keys('F10')
  end;
}

Macro {
  area="Search"; key="AltX"; flags=""; description="Exit FAR"; action = function()
Keys('F10')
  end;
}

Macro {
  area="Shell"; key="AltX"; flags=""; description="Exit FAR"; action = function()
Keys('F10')
  end;
}

Macro {
  area="Viewer"; key="AltX"; flags=""; description="Close viewer"; action = function()
Keys('F10')
  end;
}

