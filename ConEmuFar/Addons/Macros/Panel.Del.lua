Macro {
  area="Shell"; key="Del"; flags="EmptyCommandLine"; description="Use Del to remove files"; action = function()
Keys('F8')
  end;
}

Macro {
  area="Search"; key="Del"; flags=""; description="Use Del to remove files"; action = function()
Keys('F8')
  end;
}

Macro {
  area="Tree"; key="Del"; flags="EmptyCommandLine"; description="Use Del to remove files"; action = function()
Keys('F8')
  end;
}

Macro {
  area="Shell"; key="NumDel"; flags="EmptyCommandLine"; description="Use Del to remove files"; action = function()
Keys('F8')
  end;
}

Macro {
  area="Search"; key="NumDel"; flags=""; description="Use Del to remove files"; action = function()
Keys('F8')
  end;
}

Macro {
  area="Tree"; key="NumDel"; flags="EmptyCommandLine"; description="Use Del to remove files"; action = function()
Keys('F8')
  end;
}

