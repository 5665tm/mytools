Macro {
  area="Viewer"; key="Add"; flags=""; description="Move to next file and update panel position"; action = function()
Keys('Add CtrlF10 Shift')
  end;
}

Macro {
  area="Viewer"; key="Subtract"; flags=""; description="Move to previous file and update panel position"; action = function()
Keys('Subtract CtrlF10 Shift')
  end;
}

