Macro {
  area="Editor"; key="ShiftTab"; flags=""; description="^Ctrl-Tab"; action = function()
Keys('CtrlShiftTab')
  end;
}

Macro {
  area="Shell"; key="ShiftTab"; flags=""; description="^Ctrl-Tab"; action = function()
Keys('CtrlShiftTab')
  end;
}

Macro {
  area="Viewer"; key="ShiftTab"; flags=""; description="^Ctrl-Tab"; action = function()
Keys('CtrlShiftTab')
  end;
}

