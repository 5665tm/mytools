Macro {
  area="Shell"; key="Ctrl&quot;"; flags=""; description="Opening last view/edit of a file"; action = function()
Keys('AltF11 Up Enter')
  end;
}

Macro {
  area="Viewer"; key="Ctrl&quot;"; flags=""; description="Opening last view/edit of a file"; action = function()
Keys('AltF11 Up Enter')
  end;
}

Macro {
  area="Editor"; key="Ctrl&quot;"; flags=""; description="Opening last view/edit of a file"; action = function()
Keys('AltF11 Up Enter')
  end;
}

