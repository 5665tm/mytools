Macro {
  area="Editor"; key="CtrlF10"; flags=""; description="Quit from editor and position to the current file"; action = function()
Keys('CtrlF10 Esc')
  end;
}

Macro {
  area="Viewer"; key="CtrlF10"; flags=""; description="Quit from viewer and position to the current file"; action = function()
Keys('CtrlF10 Esc')
  end;
}

