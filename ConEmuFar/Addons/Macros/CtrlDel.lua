Macro {
  area="Editor"; key="CtrlDel"; flags="Selection"; description="Ctrl-Del removes selected editor block"; action = function()
Keys('CtrlD')
  end;
}

Macro {
  area="Editor"; key="CtrlNumDel"; flags="Selection"; description="Ctrl-Del removes selected editor block"; action = function()
Keys('CtrlD')
  end;
}

