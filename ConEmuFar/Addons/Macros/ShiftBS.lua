Macro {
  area="Dialog"; key="ShiftBS"; flags=""; description="Converts the word before cursor using XLat function"; action = function()
Keys('CtrlShiftLeft XLat CtrlRight')
  end;
}

Macro {
  area="Editor"; key="ShiftBS"; flags=""; description="Converts the word before cursor using XLat function"; action = function()
Keys('CtrlShiftLeft XLat CtrlRight')
  end;
}

Macro {
  area="Shell"; key="ShiftBS"; flags="NotEmptyCommandLine"; description="Converts the word before cursor using XLat function"; action = function()
Keys('CtrlShiftLeft XLat CtrlRight')
  end;
}

