Macro {
  area="Viewer"; key="CtrlDown"; flags=""; description="Scroll Down text in internal viewer"; action = function()
Keys('Down')
  end;
}

Macro {
  area="Viewer"; key="CtrlUp"; flags=""; description="Scroll Up text in internal viewer"; action = function()
Keys('Up')
  end;
}

