Macro {
  area="Editor"; key="ShiftMsWheelUp"; flags=""; description="Use Shift-MsWheel to switch between screens"; action = function()
Keys('F12 Up Enter')
  end;
}

Macro {
  area="Shell"; key="ShiftMsWheelUp"; flags=""; description="Use Shift-MsWheel to switch between screens"; action = function()
Keys('F12 Up Enter')
  end;
}

Macro {
  area="Viewer"; key="ShiftMsWheelUp"; flags=""; description="Use Shift-MsWheel to switch between screens"; action = function()
Keys('F12 Up Enter')
  end;
}

Macro {
  area="Editor"; key="ShiftMsWheelDown"; flags=""; description="Use Shift-MsWheel to switch between screens"; action = function()
Keys('F12 Down Enter')
  end;
}

Macro {
  area="Shell"; key="ShiftMsWheelDown"; flags=""; description="Use Shift-MsWheel to switch between screens"; action = function()
Keys('F12 Down Enter')
  end;
}

Macro {
  area="Viewer"; key="ShiftMsWheelDown"; flags=""; description="Use Shift-MsWheel to switch between screens"; action = function()
Keys('F12 Down Enter')
  end;
}

